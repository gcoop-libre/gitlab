# Comandos Básicos de `git`

## `git clone`

Clonar un repositorio:

```bash

  git clone https://gitlab.com/osiux/git-bash-utils/

```

Ir al directorio de la copia local del repositorio clonado:

```bash

  cd git-bash-utils

```

Ver la configuración de la copia local del repositorio clonado:

```bash

  cat .git/config

```

## `git log`

Ver el historial del repositorio clonado:

```bash

  git log

```

Ver las diferencias de los cambios en el historial del repositorio clonado:

```bash

  git log -p

```

Modificamos un archivo del repositorio:

```bash

  mcedit README.org

```

## `git status`

Vemos el estado de cambio de los archivos del repositorio clonado:

```bash

  git status

```

## `git diff`

Vemos las diferencias locales:

```bash

  git diff

```

Vemos el historial del repositorio usando `tig`:

```bash

  tig

```

## `git-alias`

Cargamos los alias del repositorio local:

```bash

  source git-alias

```

Vemos las diferencias locales usando alias `gdt`:

```bash

  gdt

```

## `git add`

Agregamos las modificaciones realizadas:

```bash

  git add README.org

```

Vemos el estado de cambio de los archivos del repositorio clonado:

```bash

  git status

```

## `git-commit`

Guardamos los cambios:

```bash

  git commit

```
