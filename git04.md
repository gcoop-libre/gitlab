# Contribuir a un proyecto

Hay 2 maneras de contribuir a un proyecto, si hay cierta confianza y/o
cercanía al equipo de desarrollo, lo mas simple es darle acceso dentro
del proyecto a quien quiera contribuir, definiendo roles y permisos,
normalmente esta opción se realiza con alguien que va a tener cierta
continuidad dentro del proyecto y que conoce el funcionamiento interno.

Ya sea por cuestiones de desconfianza, lejanía al equipo de desarrollo o
simplemente para no lidiar con roles y permisos una manera simple de
contribuir a un proyecto de manera mas separada y tal vez mas
intermitente es realizar un _fork_, es decir, crear una copia completa
del repositorio en otra cuenta y de esta manera quien contribuya tiene
libertad total sobre el _fork_ y al momento de devolver la contribución
podrá realizar un _PR_ (_pull request_) que puede ser aceptado o
rechazado sin necesidad de dar acceso.

## _fork project_

`upstream` => https://gitlab.com/gcoop-libre/gitlab

`fork` => https://gitlab.com/osiux/gitlab

Para realizar un _fork_ en _GitLab_ basta con elegir el proyecto que
queremos _"forkear"_ y presionar el botón `fork`, esto nos preguntará en
que _namespace_ (que cuenta de las que tenemos acceso) queremos hacer la
copia y además si la misma será de acceso público o privado.

También se puede realizar el _fork_ agregando `/-/forks/new` a la _URL_
del proyecto, tomando como ejemplo el proyecto `gitlab` dentro del
_namespace `gcoop-libre`, la _URL_ resultante sería la siguiente:

  https://gitlab.com/gcoop-libre/gitlab/-/forks/new

Ni bien tengamos nuestro _fork_, basta con clonarlo y modificarlo
libremente.
